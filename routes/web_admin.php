<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
*/

Route::get('/', 'IndexController@index')->name('admin');

Route::group(['prefix' => 'password'], function () {
    Route::get('/', 'PasswordController@form')->name('admin.password');
    Route::post('/save', 'PasswordController@save')->name('admin.password.save');
});

/*
|--------------------------------------------------------------------------
| MISC
|--------------------------------------------------------------------------
|
*/

Route::group(['prefix' => 'contact-info'], function () {
    Route::get('/', 'ContactInfoController@index')->name('admin.contact_info');
    Route::post('/save', 'ContactInfoController@save')->name('admin.contact_info.save');
});

/*
|--------------------------------------------------------------------------
| SITE
|--------------------------------------------------------------------------
|
*/

Route::group(['prefix' => 'just-house'], function () {
    Route::get('/', 'JustHouseController@index')->name('admin.just_house');
    Route::get('/create', 'JustHouseController@create')->name('admin.just_house.create');
    Route::get('/{id}', 'JustHouseController@edit')->name('admin.just_house.edit');
    Route::post('/store', 'JustHouseController@storeDerived')->name('admin.just_house.store');
    Route::post('/{id}/update', 'JustHouseController@updateDerived')->name('admin.just_house.update');
    Route::post('/{id}/destroy', 'JustHouseController@destroy')->name('admin.just_house.destroy');
});

Route::group(['prefix' => 'social-network'], function () {
    Route::get('/', 'SocialNetworkController@index')->name('admin.social_network');
    Route::get('/create', 'SocialNetworkController@create')->name('admin.social_network.create');
    Route::get('/{id}', 'SocialNetworkController@edit')->name('admin.social_network.edit');
    Route::post('/store', 'SocialNetworkController@store')->name('admin.social_network.store');
    Route::post('/{id}/update', 'SocialNetworkController@update')->name('admin.social_network.update');
    Route::post('/{id}/destroy', 'SocialNetworkController@destroy')->name('admin.social_network.destroy');
});

Route::group(['prefix' => 'stock-house'], function () {
    Route::get('/', 'StockHouseController@index')->name('admin.stock_house');
    Route::get('/create', 'StockHouseController@create')->name('admin.stock_house.create');
    Route::get('/{id}', 'StockHouseController@edit')->name('admin.stock_house.edit');
    Route::post('/store', 'StockHouseController@storeDerived')->name('admin.stock_house.store');
    Route::post('/{id}/update', 'StockHouseController@updateDerived')->name('admin.stock_house.update');
    Route::post('/{id}/destroy', 'StockHouseController@destroy')->name('admin.stock_house.destroy');
});

/*
|--------------------------------------------------------------------------
| System
|--------------------------------------------------------------------------
|
*/

Route::group(['prefix' => 'hints'], function () {
    Route::get('/', 'HintsController@index')->name('admin.hints');
    Route::post('/{offset}/clear', 'HintsController@clearOffset')->name('admin.hints.clear_offset');
    Route::post('/clear', 'HintsController@clear')->name('admin.hints.clear');
});
