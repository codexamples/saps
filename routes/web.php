<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
*/

Route::group([
    'middleware' => 'guest',
    'prefix' => 'login'
], function () {
    Route::get('/', 'LoginController@showLoginForm')->name('login');
    Route::post('/', 'LoginController@login');
});

Route::post('/logout', 'LoginController@logout')
    ->middleware('auth')
    ->name('logout');

Route::get('/', 'IndexController@index');
