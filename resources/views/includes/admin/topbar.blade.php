<ul class="nav navbar-nav navbar-right pull-right">
    <li class="dropdown">
        <a
                href="#"
                class="dropdown-toggle waves-effect"
                data-toggle="dropdown"
                role="button"
                aria-haspopup="true"
                aria-expanded="false"
        >
            <i class="md md-settings"></i>
        </a>
        <ul class="dropdown-menu">
            <li>
                <a href="{{ route('admin.hints') }}">
                    <i class="md md-dvr m-r-15"></i> Сохраненные теги
                </a>
            </li>
        </ul>
    </li>
</ul>