<ul>
    <?php
        /** @var  \App\Business\Support\Navigation\CurrentMenu  $current_menu */
    ?>
    <li class="text-muted menu-title">Сайт</li>
    <li>
        <a href="{{ route('admin.stock_house') }}" class="waves-effect waves-primary @if($current_menu->inLeft('stock-house', 1)) subdrop @endif">
            <i class="md md-folder-special"></i>
            <span>Акционные дома</span>
        </a>
    </li>
    <li>
        <a href="{{ route('admin.just_house') }}" class="waves-effect waves-primary @if($current_menu->inLeft('just-house', 1)) subdrop @endif">
            <i class="md md-home"></i>
            <span>Дома</span>
        </a>
    </li>
    <li>
        <a href="{{ route('admin.social_network') }}" class="waves-effect waves-primary @if($current_menu->inLeft('social_network', 1)) subdrop @endif">
            <i class="md md-public"></i>
            <span>Соц.сети</span>
        </a>
    </li>
    <li class="text-muted menu-title">Прочее</li>
    <li>
        <a href="{{ route('admin.contact_info') }}" class="waves-effect waves-primary @if($current_menu->inLeft('contact_info', 1)) subdrop @endif">
            <i class="md md-contacts"></i>
            <span>Контакт.информ.</span>
        </a>
    </li>
</ul>
