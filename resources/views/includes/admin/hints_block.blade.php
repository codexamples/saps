<?php /** @var  \App\Business\Support\TrueTranslator  $translator */ ?>
@foreach($tags as $key => $tag_list)
    @php($new_offset = empty($offset) ? $key : $offset . '.' . $key)
    @php($is_recursive = false)
    @foreach($tag_list as $item)
        @if(is_array($item))
            @php($is_recursive = true)
        @endif
        @break
    @endforeach
    @if($is_recursive)
        @include('includes.admin.hints_block', ['tags' => $tag_list, 'offset' => $new_offset, 'translator' => $translator])
    @elseif(count($tag_list))
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title"><b>{{ $translator->translate('hints.' . $new_offset) }}</b></h4>
                    <ol class="m-t-30 m-b-30">
                        @foreach($tag_list as $tag)
                            <li>{{ $tag }}</li>
                        @endforeach
                    </ol>
                    <a href="{{ route('admin.hints.clear_offset', $new_offset) }}" class="btn btn-danger pull-right destroy-btn" title="Очистить">
                        <i class="fa fa-trash"></i>
                    </a>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    @endif
@endforeach
