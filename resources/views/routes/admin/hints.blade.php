@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <p class="m-0 m-t-10 pull-left">
                    Здесь список всех ранее сохраненных тегов.
                </p>
                <a href="{{ route('admin.hints.clear') }}" class="btn btn-danger destroy-btn pull-right">
                    <i class="fa fa-trash"></i>
                    <span class="m-l-10">Очистить все</span>
                </a>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    @include('includes.admin.hints_block', ['tags' => $tags, 'offset' => '', 'translator' => $translator])
@endsection
