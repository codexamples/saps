@extends('layouts.admin')

@section('content')
    <?php /** @var  \App\Business\Data\Preferences\Section  $contact_info */ ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <form
                        class="form-horizontal"
                        method="POST"
                        action="{{ route('admin.contact_info.save') }}"
                        enctype="multipart/form-data"
                >
                    {{ csrf_field() }}
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="email">E-Mail</label>
                            <input
                                    id="email"
                                    type="text"
                                    class="form-control"
                                    name="email"
                                    value="{{ $contact_info->read('email') ?: old('email') }}"
                                    required
                                    autofocus
                            >
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="phone">Телефон</label>
                            <input
                                    id="phone"
                                    type="text"
                                    class="form-control"
                                    name="phone"
                                    value="{{ $contact_info->read('phone') ?: old('phone') }}"
                            >
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="address">Адрес</label>
                            <input
                                    id="address"
                                    type="text"
                                    class="form-control"
                                    name="address"
                                    value="{{ $contact_info->read('address') ?: old('address') }}"
                            >
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="site">Сайт</label>
                            <input
                                    id="site"
                                    type="text"
                                    class="form-control"
                                    name="site"
                                    value="{{ $contact_info->read('site') ?: old('site') }}"
                            >
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="route--personal">Маршрут: Авто</label>
                            <textarea
                                    id="route--personal"
                                    class="form-control"
                                    name="route[personal]"
                            >{!! $contact_info->read('route.personal') ?: old('route.personal') !!}</textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="route--public">Маршрут: Автобус</label>
                            <textarea
                                    id="route--public"
                                    class="form-control"
                                    name="route[public]"
                            >{!! $contact_info->read('route.public') ?: old('route.public') !!}</textarea>
                        </div>
                    </div>
                    <div class="col-md-12 m-t-20">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary pull-right">
                                <i class="fa fa-save"></i>
                            </button>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
@endsection
