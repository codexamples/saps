@extends('layouts.admin')

@section('content')
    <?php /** @var  \App\Models\SocialNetwork  $social_network */ ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <form
                        class="form-horizontal"
                        method="POST"
                        action="{{ $social_network ? route('admin.social_network.update', $social_network->id) : route('admin.social_network.store') }}"
                        enctype="multipart/form-data"
                >
                    {{ csrf_field() }}
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">Название</label>
                            <input
                                    id="name"
                                    type="text"
                                    class="form-control"
                                    name="name"
                                    value="{{ $social_network->name or old('name') }}"
                                    required
                                    autofocus
                            >
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="url">Ссылка</label>
                            <input
                                    id="url"
                                    type="text"
                                    class="form-control"
                                    name="url"
                                    value="{{ $social_network->url or old('url') }}"
                            >
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="logo">Логотип</label>
                            @if($social_network)
                                <div class="box-cont ann m-b-10">
                                    <img src="{{ $social_network->logo_url }}">
                                </div>
                            @endif
                            <input
                                    id="logo"
                                    type="file"
                                    class="form-control"
                                    name="logo"
                            >
                        </div>
                    </div>
                    <div class="col-md-12 m-t-20">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary pull-right">
                                <i class="fa fa-save"></i>
                            </button>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
@endsection
