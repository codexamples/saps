@extends('layouts.admin')

@section('content')
    <?php /** @var  \Illuminate\Pagination\LengthAwarePaginator  $social_networks */ ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="m-b-20">
                    <a href="{{ route('admin.social_network.create') }}" class="pull-right btn btn-success">
                        <i class="fa fa-plus"></i>
                    </a>
                    <div class="clearfix"></div>
                </div>
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th>Ссылка</th>
                        <th>Логотип</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($social_networks as $social_network)
                        <?php /** @var  \App\Models\SocialNetwork  $social_network */ ?>
                        <tr>
                            <td>{{ $social_network->name }}</td>
                            <td><a href="{{ $social_network->url }}" target="_blank">Перейти</a></td>
                            <td>
                                <div class="box-cont ann">
                                    <img src="{{ $social_network->logo_url }}">
                                </div>
                            </td>
                            <td>
                                <a
                                        href="{{ route('admin.social_network.edit', $social_network->id) }}"
                                        class="btn btn-info"
                                        title="Редактировать"
                                >
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a
                                        href="{{ route('admin.social_network.destroy', $social_network->id) }}"
                                        class="btn btn-danger destroy-btn"
                                        title="Удалить"
                                >
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr><td colspan="4" class="text-muted text-center">Пусто</td></tr>
                    @endforelse
                    </tbody>
                </table>
                {!! $social_networks->links() !!}
            </div>
        </div>
    </div>
@endsection
