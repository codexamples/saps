<?php /** @var  \App\Models\JustHouse  $house */ ?>
<div class="col-md-12">
    <div class="form-group">
        <label for="text">Описание</label>
        <textarea
                id="text"
                class="form-control"
                name="text"
        >{!! $house->text or old('text') !!}</textarea>
    </div>
</div>
<div class="col-md-12">
    <div class="form-group">
        <label for="technology">Технология</label>
        <input
                id="technology"
                type="text"
                class="form-control"
                data-initial-value='[{"text": "Algeria", "value" : "Algeria"}, {"text": "Angola", "value" : "Angola"}]'
                data-user-option-allowed="true"
                data-load-once="true"
                name="technology"
                value="{{ $house->technology or old('technology') }}"
                multiple
        >
    </div>
</div>
<div class="col-md-12">
    <div class="form-group">
        <label for="rooms">Спален</label>
        <input
                id="rooms"
                type="text"
                class="form-control"
                name="rooms"
                value="{{ $house->rooms or old('rooms') }}"
        >
    </div>
</div>

@section('css')
    @parent
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/tag-editor/jquery.tag-editor.css') }}">
@endsection

@section('js_bottom')
    @parent
    <script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/tag-editor/jquery.caret.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/tag-editor/jquery.tag-editor.min.js') }}"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        $('#technology').tagEditor({
          autocomplete: {
            delay: 0,
            position: {
              collision: 'flip'
            },
            source: {!! json_encode($hints['technologies']) !!}
          },
          forceLowercase: false,
          maxTags: 1
        });
      });
    </script>
@endsection