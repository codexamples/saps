<?php /** @var  \App\Models\StockHouse  $house */ ?>
<div class="col-md-12">
    <div class="form-group">
        <label for="text">Условия акции</label>
        <textarea
                id="text"
                class="form-control"
                name="text"
        >{!! $house->text or old('text') !!}</textarea>
    </div>
</div>
<div class="col-md-12">
    <div class="form-group">
        <label for="decoration">Отделка</label>
        <input
                id="decoration"
                type="text"
                class="form-control"
                data-initial-value='[{"text": "Algeria", "value" : "Algeria"}, {"text": "Angola", "value" : "Angola"}]'
                data-user-option-allowed="true"
                data-load-once="true"
                name="decoration"
                value="{{ $house->decoration or old('decoration') }}"
                multiple
        >
    </div>
</div>

@section('css')
    @parent
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/tag-editor/jquery.tag-editor.css') }}">
@endsection

@section('js_bottom')
    @parent
    <script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/tag-editor/jquery.caret.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/tag-editor/jquery.tag-editor.min.js') }}"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        $('#decoration').tagEditor({
          autocomplete: {
            delay: 0,
            position: {
              collision: 'flip'
            },
            source: {!! json_encode($hints['decorations']) !!}
          },
          forceLowercase: false,
          maxTags: 1
        });
      });
    </script>
@endsection