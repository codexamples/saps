@extends('layouts.admin')

@section('content')
    <?php /** @var  \Illuminate\Pagination\LengthAwarePaginator  $houses */ ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="m-b-20">
                    <a href="{{ route('admin.' . $config['route'] . '.create') }}" class="pull-right btn btn-success">
                        <i class="fa fa-plus"></i>
                    </a>
                    <div class="clearfix"></div>
                </div>
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th>Логотип</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($houses as $house)
                        <?php /** @var  \App\Models\House  $house */ ?>
                        <tr>
                            <td>{{ $house->title }}</td>
                            <td>
                                <div class="box-cont ann">
                                    <img src="{{ $house->image_url }}">
                                </div>
                            </td>
                            <td>
                                <a
                                        href="{{ route('admin.' . $config['route'] . '.edit', $house->id) }}"
                                        class="btn btn-info"
                                        title="Редактировать"
                                >
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a
                                        href="{{ route('admin.' . $config['route'] . '.destroy', $house->id) }}"
                                        class="btn btn-danger destroy-btn"
                                        title="Удалить"
                                >
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr><td colspan="3" class="text-muted text-center">Пусто</td></tr>
                    @endforelse
                    </tbody>
                </table>
                {!! $houses->links() !!}
            </div>
        </div>
    </div>
@endsection
