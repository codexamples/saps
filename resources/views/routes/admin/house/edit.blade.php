@extends('layouts.admin')

@section('content')
    <?php /** @var  \App\Models\House  $house */ ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <form
                        class="form-horizontal"
                        method="POST"
                        action="{{ $house ? route('admin.' . $config['route'] . '.update', $house->id) : route('admin.' . $config['route'] . '.store') }}"
                        enctype="multipart/form-data"
                >
                    {{ csrf_field() }}
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="title">Заголовок</label>
                            <input
                                    id="title"
                                    type="text"
                                    class="form-control"
                                    name="title"
                                    value="{{ $house->title or old('title') }}"
                                    required
                                    autofocus
                            >
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="image">Изображение</label>
                            @if($house)
                                <div class="box-cont ann m-b-10">
                                    <img src="{{ $house->image_url }}">
                                </div>
                            @endif
                            <input
                                    id="image"
                                    type="file"
                                    class="form-control"
                                    name="image"
                            >
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="area">Площадь</label>
                            <input
                                    id="area"
                                    type="text"
                                    class="form-control"
                                    name="area"
                                    value="{{ $house->area or old('area') }}"
                            >
                        </div>
                    </div>
                    @include('routes.admin.house.edit' . $config['include'], ['house' => $house, 'hints' => $hints])
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="position_mobile">Позиция на мобилке</label>
                            <input
                                    id="position_mobile"
                                    type="text"
                                    class="form-control"
                                    name="position_mobile"
                                    value="{{ $house->position_mobile or old('position_mobile') }}"
                            >
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="position_desktop">Позиция на десктопе</label>
                            <input
                                    id="position_desktop"
                                    type="text"
                                    class="form-control"
                                    name="position_desktop"
                                    value="{{ $house->position_desktop or old('position_desktop') }}"
                            >
                        </div>
                    </div>
                    <div class="col-md-12 m-t-20">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary pull-right">
                                <i class="fa fa-save"></i>
                            </button>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
@endsection
