<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
        <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
        <title>Saps</title>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjtWv2nBO7NfOQiUQJP0DNDstUAWGb-SM"></script>
        <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700&subset=cyrillic,cyrillic-ext">
        <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700&subset=cyrillic">
        <link href="{{ asset('css/index.css?' . $hash['css']) }}" rel="stylesheet">
        <script type="text/javascript">
            window.allJson = {!! $json !!};
        </script>
    </head>
    <body>
        <div id="app"></div>
        <script type="text/javascript" src="{{ asset('bundle.js?' . $hash['js']) }}"></script>
    </body>
</html>
