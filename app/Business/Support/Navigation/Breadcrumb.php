<?php

namespace App\Business\Support\Navigation;

/**
 * Хлебная крошка
 */
interface Breadcrumb
{
    /**
     * Возвращает ссылку хлебной крошки
     *
     * @return string
     */
    public function url();

    /**
     * Возвращает заголовк хлебной крошки
     *
     * @return string
     */
    public function caption();

    /**
     * Возвращает экземпляр самой первой хлебной крошки
     *
     * @return static
     */
    public function first();

    /**
     * Возвращает true, если данный экземпляр - самый первый
     *
     * @return bool
     */
    public function isFirst();

    /**
     * Возвращает экземпляр следующей хлебной крошки после текущей
     *
     * @return static|null
     */
    public function next();

    /**
     * Возвращает true, если у текущей хлебной крошки есть следующая
     *
     * @return bool
     */
    public function hasNext();

    /**
     * Ищет и возвращает самую последнюю хлебную крошку
     *
     * @return static
     */
    public function last();

    /**
     * Добавляет новую хлебную крошку в конец
     *
     * @param  string  $url
     * @param  string  $caption
     * @return static
     */
    public function append(string $url, string $caption);
}
