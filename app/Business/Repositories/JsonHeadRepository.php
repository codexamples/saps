<?php

namespace App\Business\Repositories;

use Illuminate\Http\Request;

/**
 * Репозиторий выгрузки JSON в шапку сайта
 */
interface JsonHeadRepository
{
    /**
     * Возвращает выгрузку JSON для шапки сайта
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $options
     * @return string
     */
    public function combine(Request $request = null, int $options = 0);
}
