<?php

namespace App\Business\Data\Preferences;

use Exception;
use Throwable;

/**
 * Секция настроек не найдена
 */
class SectionNotFoundException extends Exception
{
    /**
     * Создает экземпляр исключения
     *
     * @param  string  $key
     * @param  int  $code
     * @param  \Throwable  $previous
     */
    public function __construct(string $key, int $code = 0, Throwable $previous = null)
    {
        parent::__construct('Settings section "' . $key . '" not found', $code, $previous);
    }
}
