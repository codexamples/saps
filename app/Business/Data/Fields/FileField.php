<?php

namespace App\Business\Data\Fields;

use Illuminate\Support\Facades\Storage;
use ReflectionClass;

/**
 * Доступ к файловым полям
 */
trait FileField
{
    /**
     * Возвращает файловое хранилище
     *
     * @return \Illuminate\Filesystem\FilesystemAdapter
     */
    protected function getFileStorage()
    {
        $reflection = new ReflectionClass(get_class($this));

        return Storage::drive(kebab_case($reflection->getShortName()));
    }

    /**
     * Возвращает ссылку на файловое поле
     *
     * @param  string  $field
     * @return string
     */
    public function getFileUrl(string $field)
    {
        return $this->getFileStorage()->url($this->getFieldValue($field));
    }
}
