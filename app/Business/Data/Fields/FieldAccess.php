<?php

namespace App\Business\Data\Fields;

/**
 * Универсальный доступ к полям
 */
trait FieldAccess
{
    /**
     * Возвращает значение поля
     *
     * @param  string  $field
     * @return mixed
     * @throws \App\Business\Data\Fields\FieldNotFoundException
     */
    protected function getFieldValue(string $field)
    {
        if (property_exists($this, $field)) {
            return $this->$field;
        }

        $long_condition = property_exists($this, 'attributes')
            && is_array($this->attributes)
            && array_key_exists($field, $this->attributes);

        if ($long_condition) {
            return $this->attributes[$field];
        }

        throw new FieldNotFoundException($field);
    }

    /**
     * Задает значение поля
     *
     * @param  string  $field
     * @param  $value
     * @return void
     * @throws \App\Business\Data\Fields\FieldNotFoundException
     */
    protected function setFieldValue(string $field, $value)
    {
        if (property_exists($this, $field)) {
            $this->$field = $value;
        } elseif (property_exists($this, 'attributes') && is_array($this->attributes)) {
            $this->attributes[$field] = $value;
        } else {
            throw new FieldNotFoundException($field);
        }
    }
}
