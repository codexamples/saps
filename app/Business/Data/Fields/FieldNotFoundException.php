<?php

namespace App\Business\Data\Fields;

use Exception;
use Throwable;

/**
 * Поле не найдено
 */
class FieldNotFoundException extends Exception
{
    /**
     * Создает экземпляр исключения
     *
     * @param  string  $field
     * @param  int  $code
     * @param  \Throwable  $previous
     */
    public function __construct(string $field, int $code = 0, Throwable $previous = null)
    {
        parent::__construct('Field "' . $field . '" not found', $code, $previous);
    }
}
