<?php

namespace App\Business\Http;

use Illuminate\Http\Request;

/**
 * API-клиент для сервиса https://mailer.***.ru
 */
interface MailerApi
{
    /**
     * Отправляет заявку через почтовый сервис от формы обратной связи
     * Возвращает некоторые данные ответа от сервера
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function sendFeedback(Request $request);
}
