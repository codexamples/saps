<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \App\Business\Support\Navigation\Breadcrumb::class,
            function () {
                return \App\Support\Navigation\Breadcrumb::start(route('admin'), config('app.name', 'Laravel'));
            }
        );

        $this->app->bind(
            \App\Business\Support\Navigation\CurrentMenu::class,
            function () {
                return \App\Support\Navigation\CurrentMenu::start()->addLeft('index');
            }
        );

        $this->app->bind(
            \App\Business\Data\Preferences\Settings::class,
            \App\Models\Preferences\Settings::class
        );

        $this->app->bind(
            \App\Business\Http\MailerApi::class,
            \App\Http\External\MailerApi::class
        );

        $this->app->bind(
            \App\Business\Support\FastValidator::class,
            \App\Support\FastValidator::class
        );

        $this->app->bind(
            \App\Business\Support\TrueTranslator::class,
            \App\Support\TrueTranslator::class
        );
    }
}
