<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Более гибкая настройка роутов "пакетированно"
     *
     * @var array
     */
    protected $packages = [
        [
            'prefix' => 'api',
            'middleware' => 'api',
            'namespace' => 'Api',
            'group' => 'api',
        ],
        [
            'prefix' => 'admin',
            'middleware' => ['web', 'auth'],
            'namespace' => 'Admin',
            'group' => 'admin',
        ],
        [
            'prefix' => null,
            'middleware' => 'web',
            'namespace' => 'Web',
            'group' => null,
        ],
    ];

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapRoutes();
        //
    }

    /**
     * Объявляет пакеты роутов из соответствующего массива
     *
     * @return void
     */
    protected function mapRoutes()
    {
        foreach ($this->packages as $package) {
            $route = Route::middleware($package['middleware']);

            if ($package['prefix'] !== null) {
                $route->prefix($package['prefix']);
            }

            $route
                ->namespace($this->namespace . ($package['namespace'] === null ? '' : '\\' . $package['namespace']))
                ->group(base_path('routes/web' . ($package['group'] === null ? '' : '_' . $package['group']) . '.php'));
        }
    }
}
