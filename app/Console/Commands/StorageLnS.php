<?php

namespace App\Console\Commands;

class StorageLnS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'storage:lns {folder}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Makes symbolic link from public folder to its storage analog';

    /**
     * Тоже самое, что и handle, только ничего не возвращает и IDE-шка не делает мозги, подсвечивая закрывающую скобку
     *
     * @return void
     */
    protected function perform()
    {
        $folder = $this->argument('folder');

        $from = public_path('file/' . $folder);
        $to = storage_path('app/' . $folder);

        if (file_exists($from)) {
            unlink($from);
        }

        symlink($to, $from);
        chmod($from, 0777);

        $this->info('Made symbolic link from "' . $from . '" to "' . $to . '"');
    }
}
