<?php

namespace App\Console\Commands;

use Illuminate\Console\Command as BaseCommand;
use Illuminate\Support\Facades\Log;

abstract class Command extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = null;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = null;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->perform();
        return 0;
    }

    /**
     * Тоже самое, что и handle, только ничего не возвращает и IDE-шка не делает мозги, подсвечивая закрывающую скобку
     *
     * @return void
     */
    abstract protected function perform();

    /**
     * Write a string as information output.
     *
     * @param  string  $string
     * @param  null|int|string  $verbosity
     * @return void
     */
    public function info($string, $verbosity = null)
    {
        Log::info($this->signature . ': ' . $string);
        parent::info($string, $verbosity);
    }
}
