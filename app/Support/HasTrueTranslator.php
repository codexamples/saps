<?php

namespace App\Support;

use App\Business\Support\TrueTranslator as TrueTranslatorContract;

/**
 * Класс, имеющий правильный переводчик, как синглтон
 */
trait HasTrueTranslator
{
    /**
     * Правильный переводчик
     *
     * @var \App\Business\Support\TrueTranslator
     */
    private $trueTranslator;

    /**
     * Возвращает правильный переводчик
     *
     * @return \App\Business\Support\TrueTranslator
     */
    protected function trueTranslator()
    {
        if ($this->trueTranslator === null) {
            $this->trueTranslator = app()->make(TrueTranslatorContract::class);
        }

        return $this->trueTranslator;
    }
}
