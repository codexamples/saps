<?php

namespace App\Support;

use App\Business\Support\TrueTranslator as TrueTranslatorContract;
use Illuminate\Support\Facades\Lang;

/**
 * Реализация правильного переводчика
 */
class TrueTranslator implements TrueTranslatorContract
{
    /**
     * Возвращает последний кусок ключа
     *
     * @param  string  $key
     * @return string
     */
    protected function getLastKeyPart(string $key)
    {
        $parts = explode('.', $key);
        return array_pop($parts);
    }

    /**
     * Возвращает true, если перевод имеется в наличии
     *
     * @param  string  $key
     * @return bool
     */
    public function hasTranslation(string $key)
    {
        return Lang::has($key);
    }

    /**
     * Возвращает перевод по указанному ключу, возвращая последний кусок ключа, если перевода нет
     *
     * @param  string  $key
     * @param  array  $replace
     * @param  $locale
     * @return string
     */
    public function translate(string $key, array $replace = [], $locale = null)
    {
        return $this->hasTranslation($key)
            ? trans($key, $replace, $locale)
            : $this->getLastKeyPart($key);
    }

    /**
     * Возвращает перевод на основе числа, возвращая последний кусок ключа, если перевода нет
     *
     * @param  string  $key
     * @param  $number
     * @param  array  $replace
     * @param  $locale
     * @return string
     */
    public function translateChoice(string $key, $number, array $replace = [], $locale = null)
    {
        return $this->hasTranslation($key)
            ? trans_choice($key, $number, $replace, $locale)
            : $this->getLastKeyPart($key);
    }

    /**
     * Возвращает перевод по указанному ключу, возвращая последний кусок ключа, если перевода нет
     *
     * @param  string  $key
     * @param  array  $replace
     * @param  $locale
     * @return string
     */
    public function translateJson(string $key, array $replace = [], $locale = null)
    {
        return $this->hasTranslation($key)
            ? __($key, $replace, $locale)
            : $this->getLastKeyPart($key);
    }
}
