<?php

namespace App\Http\External;

use App\Business\Http\MailerApi as MailerApiContract;
use App\Models\Preferences\HasSettings;
use Illuminate\Http\Request;

/**
 * Реализация API-клиента для сервиса https://mailer.***.ru
 */
class MailerApi implements MailerApiContract
{
    use HasSettings;

    /**
     * Отправляет заявку через почтовый сервис от формы обратной связи
     * Возвращает некоторые данные ответа от сервера
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function sendFeedback(Request $request)
    {
        $none = '<i>none</i>';
        $msg = [
            '<p>',
            '<b>Дата:</b> ' . date('d.m.Y H:i:s'),
            '<br>',
            '<b>Имя:</b> ' . $request->input('name', $none),
            '<br>',
            '<b>E-Mail / Телефон:</b> ' . $request->input('email', $none),
            '<br>',
            '<b>Телефон:</b> ' . $request->input('phone', $none),
            '<br>',
            '<b>Страница:</b> ' . (array_key_exists('HTTP_REFERER', $_SERVER) ? $_SERVER['HTTP_REFERER'] : ''),
            '</p>',
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://mailer.***.ru/***/***');
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([
            'subject'   => 'Обратная связь',
            'text'      => implode(' ', $msg),
            'mailTo'    => [
                $this->settings()->loadSection('contact_info')->read('email'),
            ],
            'userAgent' => array_key_exists('HTTP_USER_AGENT', $_SERVER) ? $_SERVER['HTTP_USER_AGENT'] : '',
            'ip'        => array_key_exists('REMOTE_ADDR', $_SERVER) ? $_SERVER['REMOTE_ADDR'] : '',
        ]));

        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        header('Access-Control-Allow-Origin: *');
        return ['http_code' => $code];
    }
}
