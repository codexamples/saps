<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class HouseCrudRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:64',
            'text' => 'required|max:1023',
            'image' => ($this->id ? 'nullable' : 'required') . '|max:10240|file',
            'area' => 'required|numeric|between:0,4096',
            'position_mobile' => 'nullable|max:32',
            'position_desktop' => 'nullable|max:32',
        ];
    }
}
