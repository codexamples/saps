<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class SocialNetworkCrudRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:32',
            'url' => 'required|max:255|url',
            'logo' => ($this->id ? 'nullable' : 'required') . '|max:10240|file',
        ];
    }
}
