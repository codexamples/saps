<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class ContactInfoSaveRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|max:127',
            'phone' => 'required|max:32',
            'address' => 'required|max:255',
            'site' => 'required|url|max:255',
            'route.personal' => 'required|max:1023',
            'route.public' => 'required|max:1023',
        ];
    }
}
