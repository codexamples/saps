<?php

namespace App\Http\Requests\Admin;

class StockHouseCrudRequest extends HouseCrudRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();

        $rules['decoration'] = 'required|max:32';

        return $rules;
    }
}
