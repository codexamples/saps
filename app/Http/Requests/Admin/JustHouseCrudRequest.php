<?php

namespace App\Http\Requests\Admin;

class JustHouseCrudRequest extends HouseCrudRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();

        $rules['technology'] = 'required|max:32';
        $rules['rooms'] = 'required|integer|between:0,127';

        return $rules;
    }
}
