<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class AdminAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     */
    public function handle($request, Closure $next)
    {
        $result = $next($request);

        /** @var  \App\Business\Authorization\AdminAccessible  $auth */
        $auth = Auth::user();

        if ($auth->isAdmin()) {
            return $result;
        }

        throw new AccessDeniedHttpException();
    }
}
