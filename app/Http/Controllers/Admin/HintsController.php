<?php

namespace App\Http\Controllers\Admin;

use App\Models\Preferences\HasSettings;
use App\Support\HasTrueTranslator;

class HintsController extends Controller
{
    use HasSettings, HasTrueTranslator;

    /**
     * Список всех ранее сохраненных тегов
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->breadcrumb()->append(route('admin.hints'), 'Сохраненные теги');
        $this->currentMenu()->addLeft('hints');

        return view('routes.admin.hints', $this->fillViewData([
            'tags' => $this->settings()->loadSection('hints')->readAll(),
            'translator' => $this->trueTranslator(),
        ]));
    }

    /**
     * Очищает ранее сохраненные теги по указанному адресу
     *
     * @param  string  $offset
     * @return \Illuminate\Http\Response
     */
    public function clearOffset(string $offset)
    {
        $settings = $this->settings();
        $section = $settings->loadSection('hints');

        $settings->saveSection(
            $section->remove($offset)
        );

        return redirect()->route('admin.hints')->with('status', 'Список сохраненных тегов очищен');
    }

    /**
     * Очищает все ранее добавленные теги
     *
     * @return \Illuminate\Http\Response
     */
    public function clear()
    {
        $this->settings()->removeSection('hints');

        return redirect()->route('admin.hints')->with('status', 'Список всех тегов очищен');
    }
}
