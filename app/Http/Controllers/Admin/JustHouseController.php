<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\JustHouseCrudRequest;
use App\Models\Repositories\JustHouseRepository;

class JustHouseController extends HouseController
{
    /**
     * Конфигурация контроллера
     *
     * @var array
     */
    protected $config = [
        'route' => 'just_house',
        'view' => 'house',
        'type' => '',
        'menu' => 'just-house',
        'include' => '__just',
    ];

    /**
     * Создает новый экземпляр контроллера
     *
     * @param  \App\Models\Repositories\JustHouseRepository  $houses
     */
    public function __construct(JustHouseRepository $houses)
    {
        parent::__construct($houses);
    }

    /**
     * Создание дома
     *
     * @param  \App\Http\Requests\Admin\JustHouseCrudRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function storeDerived(JustHouseCrudRequest $request)
    {
        return $this->store($request);
    }

    /**
     * Обновление дома
     *
     * @param  \App\Http\Requests\Admin\JustHouseCrudRequest  $request
     * @param  string  $hash
     * @return \Illuminate\Http\Response
     */
    public function updateDerived(JustHouseCrudRequest $request, string $hash)
    {
        return $this->update($request, $hash);
    }
}
