<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\SocialNetworkCrudRequest;
use App\Models\Repositories\SocialNetworkRepository;

class SocialNetworkController extends Controller
{
    /**
     * Репозиторий социальных сетей
     *
     * @var \App\Models\Repositories\SocialNetworkRepository
     */
    private $socialNetworks;

    /**
     * Создает новый экземпляр контроллера
     *
     * @param  \App\Models\Repositories\SocialNetworkRepository  $social_networks
     */
    public function __construct(SocialNetworkRepository $social_networks)
    {
        $this->breadcrumb()->append(route('admin.social_network'), 'Соц.Сети');
        $this->currentMenu()->addLeft('social_network');

        $this->socialNetworks = $social_networks;
    }

    /**
     * Возвращает репозиторий социальных сетей
     *
     * @return \App\Models\Repositories\SocialNetworkRepository
     */
    protected function socialNetworks()
    {
        return $this->socialNetworks;
    }

    /**
     * Список социальных сетей
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('routes.admin.social_network.index', $this->fillViewData([
            'social_networks' => $this->socialNetworks()->all(),
        ]));
    }

    /**
     * Форма создания социальной сети
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->breadcrumb()->append(route('admin.social_network.create'), 'Новая соц.сеть');

        return view('routes.admin.social_network.edit', $this->fillViewData([
            'social_network' => null,
        ]));
    }

    /**
     * Форма редактирования социальной сети
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $socialNetwork = $this->socialNetworks()->one($id);

        $this->breadcrumb()->append(route('admin.social_network.edit', $socialNetwork->id), $socialNetwork->name);

        return view('routes.admin.social_network.edit', $this->fillViewData([
            'social_network' => $socialNetwork,
        ]));
    }

    /**
     * Создание социальной сети
     *
     * @param  \App\Http\Requests\Admin\SocialNetworkCrudRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SocialNetworkCrudRequest $request)
    {
        $socialNetwork = $this->socialNetworks()->create($request);

        return redirect()->route('admin.social_network.edit', $socialNetwork->id)->with('status', 'Соц.сеть создана');
    }

    /**
     * Обновление социальной сети
     *
     * @param  \App\Http\Requests\Admin\SocialNetworkCrudRequest  $request
     * @param  string  $hash
     * @return \Illuminate\Http\Response
     */
    public function update(SocialNetworkCrudRequest $request, string $hash)
    {
        $socialNetwork = $this->socialNetworks()->update($hash, $request);

        return redirect()->route('admin.social_network.edit', $socialNetwork->id)->with('status', 'Соц.сеть обновлена');
    }

    /**
     * Удаление социальной сети
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $this->socialNetworks()->destroy($id);

        return redirect()->route('admin.social_network')->with('status', 'Соц.сеть удалена');
    }
}
