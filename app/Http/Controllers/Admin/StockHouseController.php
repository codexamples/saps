<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\StockHouseCrudRequest;
use App\Models\Repositories\StockHouseRepository;

class StockHouseController extends HouseController
{
    /**
     * Конфигурация контроллера
     *
     * @var array
     */
    protected $config = [
        'route' => 'stock_house',
        'view' => 'house',
        'type' => ' (акцион.)',
        'menu' => 'stock-house',
        'include' => '__stock',
    ];

    /**
     * Создает новый экземпляр контроллера
     *
     * @param  \App\Models\Repositories\StockHouseRepository  $houses
     */
    public function __construct(StockHouseRepository $houses)
    {
        parent::__construct($houses);
    }

    /**
     * Создание дома
     *
     * @param  \App\Http\Requests\Admin\StockHouseCrudRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function storeDerived(StockHouseCrudRequest $request)
    {
        return $this->store($request);
    }

    /**
     * Обновление дома
     *
     * @param  \App\Http\Requests\Admin\StockHouseCrudRequest  $request
     * @param  string  $hash
     * @return \Illuminate\Http\Response
     */
    public function updateDerived(StockHouseCrudRequest $request, string $hash)
    {
        return $this->update($request, $hash);
    }
}
