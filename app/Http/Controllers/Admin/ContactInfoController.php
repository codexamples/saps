<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\ContactInfoSaveRequest;
use App\Models\Preferences\HasSettings;

class ContactInfoController extends Controller
{
    use HasSettings;

    /**
     * Форма редактирования настроек контактных данных
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->breadcrumb()->append(route('admin.contact_info'), 'Контактная информация');
        $this->currentMenu()->addLeft('contact_info');

        return view('routes.admin.contact_info', $this->fillViewData([
            'contact_info' => $this->settings()->loadSection('contact_info'),
        ]));
    }

    /**
     * Сохранение настроек контактных данных
     *
     * @param  \App\Http\Requests\Admin\ContactInfoSaveRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function save(ContactInfoSaveRequest $request)
    {
        $settings = $this->settings();
        $settings->saveSection($settings->loadSection('contact_info')->fill($request->all()));

        return redirect()->route('admin.contact_info')->with('status', 'Контактная информация обновлена');
    }
}
