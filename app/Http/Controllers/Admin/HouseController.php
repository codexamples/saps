<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\HouseCrudRequest;
use App\Models\Repositories\HouseRepository;

class HouseController extends Controller
{
    /**
     * Репозиторий домов
     *
     * @var \App\Models\Repositories\HouseRepository
     */
    private $houses;

    /**
     * Конфигурация контроллера
     *
     * @var array
     */
    protected $config = [
        'route' => 'house',
        'view' => 'house',
        'type' => '',
        'menu' => 'house',
        'include' => '',
    ];

    /**
     * Создает новый экземпляр контроллера
     *
     * @param  \App\Models\Repositories\HouseRepository  $houses
     */
    public function __construct(HouseRepository $houses)
    {
        $this->breadcrumb()->append(route('admin.' . $this->config['route']), 'Дома' . $this->config['type']);
        $this->currentMenu()->addLeft($this->config['menu']);

        $this->houses = $houses;
    }

    /**
     * Возвращает репозиторий домов
     *
     * @return \App\Models\Repositories\HouseRepository
     */
    protected function houses()
    {
        return $this->houses;
    }

    /**
     * Список домов
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('routes.admin.' . $this->config['view'] . '.index', $this->fillViewData([
            'houses' => $this->houses()->all(),
            'config' => $this->config,
        ]));
    }

    /**
     * Форма создания дома
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->breadcrumb()->append(
            route('admin.' . $this->config['route'] . '.create'),
            'Новый дом' . $this->config['type']
        );

        return view('routes.admin.' . $this->config['view'] . '.edit', $this->fillViewData([
            'house' => null,
            'config' => $this->config,
            'hints' => $this->houses()->getAvailableHints(),
        ]));
    }

    /**
     * Форма редактирования дома
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $house = $this->houses()->one($id);

        $this->breadcrumb()->append(route('admin.' . $this->config['route'] . '.edit', $house->id), $house->title);

        return view('routes.admin.' . $this->config['view'] . '.edit', $this->fillViewData([
            'house' => $house,
            'config' => $this->config,
            'hints' => $this->houses()->getAvailableHints(),
        ]));
    }

    /**
     * Создание дома
     *
     * @param  \App\Http\Requests\Admin\HouseCrudRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HouseCrudRequest $request)
    {
        $house = $this->houses()->create($request);

        return redirect()
            ->route('admin.' . $this->config['route'] . '.edit', $house->id)
            ->with('status', 'Дом' . $this->config['type'] . ' создан');
    }

    /**
     * Обновление дома
     *
     * @param  \App\Http\Requests\Admin\HouseCrudRequest  $request
     * @param  string  $hash
     * @return \Illuminate\Http\Response
     */
    public function update(HouseCrudRequest $request, string $hash)
    {
        $house = $this->houses()->update($hash, $request);

        return redirect()
            ->route('admin.' . $this->config['route'] . '.edit', $house->id)
            ->with('status', 'Дом' . $this->config['type'] . ' обновлен');
    }

    /**
     * Удаление дома
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $this->houses()->destroy($id);

        return redirect()
            ->route('admin.' . $this->config['route'])
            ->with('status', 'Дом' . $this->config['type'] . ' удален');
    }
}
