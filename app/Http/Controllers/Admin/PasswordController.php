<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\PasswordSaveRequest;
use App\Support\HasFastValidator;
use Illuminate\Support\Facades\Auth;

class PasswordController extends Controller
{
    use HasFastValidator;

    /**
     * Форма смены админского пароля
     *
     * @return \Illuminate\Http\Response
     */
    public function form()
    {
        $this->breadcrumb()->append(route('admin.password'), 'Пароль');
        $this->currentMenu()->addLeft('password');

        return view('routes.admin.password', $this->fillViewData());
    }

    /**
     * Изменение админского пароля
     *
     * @param  \App\Http\Requests\Admin\PasswordSaveRequest  $request
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function save(PasswordSaveRequest $request)
    {
        /** @var  \App\Business\Authorization\PasswordAccessible  $auth */
        $auth = Auth::user();

        if ($auth->passwordAsserts($request->input('old_password'))) {
            $auth->changePassword($request->input('password'));

            return redirect()->route('admin.password')->with('status', 'Пароль сменен');
        }

        return redirect()->route('admin.password')->withErrors($this->fastValidator()->makeErroneousValidator([
            'password.wrong' => 'Старый пароль неверный',
        ]));
    }
}
