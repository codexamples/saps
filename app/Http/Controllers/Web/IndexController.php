<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Repositories\JsonHead\MainRepository;

class IndexController extends Controller
{
    /**
     * Главная страница a.k.a. React-Based
     *
     * @param  \App\Models\Repositories\JsonHead\MainRepository  $main_models_json
     * @return \Illuminate\Http\Response
     */
    public function index(MainRepository $main_models_json)
    {
        return view('routes.web.index', [
            'json' => $main_models_json->combine(),
            'hash' => [
                'css' => md5_file(public_path('css/index.css')),
                'js' => md5_file(public_path('bundle.js')),
            ],
        ]);
    }
}
