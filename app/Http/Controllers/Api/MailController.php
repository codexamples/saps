<?php

namespace App\Http\Controllers\Api;

use App\Business\Http\MailerApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MailController extends Controller
{
    /**
     * Отправка по почте обратной связи
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Business\Http\MailerApi  $mailer_api
     * @return \Illuminate\Http\Response
     */
    public function send(Request $request, MailerApi $mailer_api)
    {
        $result = $mailer_api->sendFeedback($request);

        return response()->json(
            $result,
            array_key_exists('http_code', $result) ? $result['http_code'] : 200
        );
    }
}
