<?php

namespace App\Models;

use App\Business\Data\Fields\FieldAccess;
use App\Business\Data\Fields\FileField;
use Illuminate\Database\Eloquent\Model;

class SocialNetwork extends Model
{
    use FieldAccess, FileField;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'url',
        'logo',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'logo',
        'created_at',
        'updated_at',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'logo_url',
    ];

    /**
     * Возвращает ссылку на логотип
     *
     * @return string
     */
    public function getLogoUrlAttribute()
    {
        return $this->getFileUrl('logo');
    }
}
