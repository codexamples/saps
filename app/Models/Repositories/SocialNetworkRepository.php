<?php

namespace App\Models\Repositories;

/**
 * Репозиторий социальных сетей
 */
class SocialNetworkRepository extends UploadedFilesRepository
{
    /**
     * Список ключей для файлов
     *
     * @return array
     */
    protected $fileKeys = [
        'logo',
    ];
}
