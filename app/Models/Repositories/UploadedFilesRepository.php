<?php

namespace App\Models\Repositories;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

/**
 * Репозиторий моделей, которые имеют загружаемые файлы
 */
class UploadedFilesRepository extends Repository
{
    /**
     * Список ключей для файлов
     *
     * @return array
     */
    protected $fileKeys = [];

    /**
     * Возвращает хранилище файлов, выделенное под модель
     *
     * @return \Illuminate\Filesystem\FilesystemAdapter
     */
    protected function getFileStorage()
    {
        return Storage::drive(kebab_case(basename(str_replace('\\', '/', $this->modelClass))));
    }

    /**
     * Удаляет файл из хранилища
     *
     * @param  string  $path
     * @return bool
     */
    protected function unlinkFile(string $path)
    {
        return $this->getFileStorage()->exists($path) && $this->getFileStorage()->delete($path);
    }

    /**
     * Загружает файл, возвращает его новый путь
     *
     * @param  \Illuminate\Http\UploadedFile  $file
     * @return string
     */
    protected function uploadFile(UploadedFile $file)
    {
        $path = $this->generateFileName() . '.' . mb_strtolower($file->getClientOriginalExtension());
        $this->getFileStorage()->put($path, file_get_contents($file->getRealPath()));

        return $path;
    }

    /**
     * Генерирует имя для нового файла
     *
     * @return string
     */
    protected function generateFileName()
    {
        return md5(microtime());
    }

    /**
     * Создает новую запись в моделе, возвращает ее экземпляр
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(Request $request)
    {
        $all = $request->all();

        foreach ($this->fileKeys as $key) {
            $file = $request->file($key);

            if ($file) {
                Arr::set($all, $key, $this->uploadFile($file));
            } else {
                Arr::forget($all, $key);
            }
        }

        $request = new Request();
        $request->merge($all);

        return parent::create($request);
    }

    /**
     * Обновляет данные записи модели, возвращает ее экземпляр
     *
     * @param  $key
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($key, Request $request)
    {
        $model = $this->one($key);
        $all = $request->all();

        foreach ($this->fileKeys as $key) {
            $file = $request->file($key);

            if ($file) {
                $this->unlinkFile($model->$key);

                Arr::set($all, $key, $this->uploadFile($file));
            } else {
                Arr::forget($all, $key);
            }
        }

        $model->fill($all)->save();
        return $model;
    }

    /**
     * Удаляет запись модели
     *
     * @param  $key
     * @return bool|null
     */
    public function destroy($key)
    {
        $model = $this->one($key);

        foreach ($this->fileKeys as $key) {
            $this->unlinkFile($model->$key);
        }

        return $model->delete();
    }
}
