<?php

namespace App\Models\Repositories;

/**
 * Репозиторий акционных домов
 */
class StockHouseRepository extends HouseRepository
{
    /**
     * Значение для поля акции
     *
     * @var mixed
     */
    protected $stockValue = 1;

    /**
     * Значения полей по умолчанию
     *
     * @var array
     */
    protected $defaultValues = [
        'technology' => '',
        'rooms' => 0,
    ];

    /**
     * Ключи для подсказок
     */
    protected $hintOffsets = [
        'decoration' => 'decorations',
    ];
}
