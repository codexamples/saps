<?php

namespace App\Models\Repositories;

use App\Models\Preferences\HasSettings;
use Illuminate\Http\Request;

/**
 * Репозиторий домов
 */
class HouseRepository extends UploadedFilesRepository
{
    use HasSettings;

    /**
     * Список ключей для файлов
     *
     * @return array
     */
    protected $fileKeys = [
        'image',
    ];

    /**
     * Значение для поля акции
     *
     * @var mixed
     */
    protected $stockValue = null;

    /**
     * Значения полей по умолчанию
     *
     * @var array
     */
    protected $defaultValues = [];

    /**
     * Ключи для подсказок
     */
    protected $hintOffsets = [];

    /**
     * Дополняет запрос
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function prepareRequest(Request $request)
    {
        $request->merge(array_merge(['stock' => $this->stockValue], $this->defaultValues));
    }

    /**
     * Возвращает список доступных подсказок
     *
     * @return array
     */
    public function getAvailableHints()
    {
        $section = $this->settings()->loadSection('hints');
        $arr = [];

        foreach ($this->hintOffsets as $offset) {
            $arr[$offset] = $section->read($offset);
        }

        return $arr;
    }

    /**
     * Добавляет новые доступные подсказки
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function appendAvailableHint(Request $request)
    {
        $arr = $this->getAvailableHints();

        foreach ($this->hintOffsets as $key => $offset) {
            $hint = $request->input($key, '');

            if ($hint && !in_array($hint, $arr[$offset])) {
                $arr[$offset][] = $hint;
            }
        }

        $section = $this->settings()->loadSection('hints');

        foreach ($arr as $k => $v) {
            $section->write($k, $v);
        }

        $this->settings()->saveSection($section);
    }

    /**
     * Стартует запрос для выборки всех моделей
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function queryAll(Request $request = null)
    {
        return parent::queryAll($request)->where('stock', $this->stockValue)->orderBy('updated_at', 'desc');
    }

    /**
     * Создает новую запись в моделе, возвращает ее экземпляр
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(Request $request)
    {
        $this->prepareRequest($request);
        $this->appendAvailableHint($request);

        return parent::create($request);
    }

    /**
     * Обновляет данные записи модели, возвращает ее экземпляр
     *
     * @param  $key
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($key, Request $request)
    {
        $this->prepareRequest($request);
        $this->appendAvailableHint($request);

        return parent::update($key, $request);
    }
}
