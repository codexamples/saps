<?php

namespace App\Models\Repositories;

use Illuminate\Http\Request;

/**
 * Репозиторий обычных домов
 */
class JustHouseRepository extends HouseRepository
{
    /**
     * Значение для поля акции
     *
     * @var mixed
     */
    protected $stockValue = 0;

    /**
     * Значения полей по умолчанию
     *
     * @var array
     */
    protected $defaultValues = [
        'decoration' => '',
    ];

    /**
     * Ключи для подсказок
     */
    protected $hintOffsets = [
        'technology' => 'technologies',
    ];

    /**
     * Стартует запрос для выборки всех моделей
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function queryAll(Request $request = null)
    {
        return $this->startQuery()->where('stock', $this->stockValue);
    }
}
