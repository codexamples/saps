<?php

namespace App\Models\Repositories\JsonHead;

use App\Business\Repositories\JsonHeadRepository;
use App\Business\Repositories\ModelRepository;
use App\Models\Preferences\HasSettings;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use ReflectionClass;

/**
 * Реализация репозитория для JSON-выгрузок в шапку сайта
 */
class Repository implements JsonHeadRepository
{
    use HasSettings;

    /**
     * Список классов репозиториев моделей
     *
     * @var array
     */
    protected $modelRepoClasses = [];

    /**
     * Псевдонимы названий моделей
     *
     * @var array
     */
    protected $modelAlias = [];

    /**
     * Список названий секций для автовыгрузки
     *
     * @var array
     */
    protected $settingsSections = [];

    /**
     * Список псевдонимов для названий секций настроек
     *
     * @var array
     */
    protected $settingsSectionAlias = [];

    /**
     * Список отдельных ключей из настроек
     *
     * @var array
     */
    protected $settingsOnly = [];

    /**
     * Мутаторы значений настроек
     *
     * @var array
     */
    protected $settingsMutators = [];

    /**
     * Возвращает выгрузку JSON для шапки сайта
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $options
     * @return string
     */
    public function combine(Request $request = null, int $options = 0)
    {
        $arr = [];

        $this->combineModels($arr, $request);
        $this->combineSettings($arr);

        return json_encode($arr, $options);
    }

    /**
     * Заполняет моделями
     *
     * @param  array  &$arr
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function combineModels(array &$arr, Request $request = null)
    {
        foreach ($this->modelRepoClasses as $class) {
            $reflection = new ReflectionClass($class);
            $short = $reflection->getShortName();

            if (ends_with($short, 'Repository')) {
                $parts = explode('_', snake_case($short));
                array_pop($parts);

                $model = implode('_', $parts);
            } else {
                $model = $short;
            }

            if (array_key_exists($model, $this->modelAlias)) {
                $model = $this->modelAlias[$model];
            }

            $arr[$model] = [];

            if (class_exists($class, true)) {
                /** @var  \App\Business\Repositories\ModelRepository  $repo */
                $repo = app()->make($class);

                if ($repo instanceof ModelRepository) {
                    $subreq = new Request();

                    if ($request && $request->has($model)) {
                        $value = $request->input($model);
                        $subreq->merge(is_array($value) ? $value : [$model => $value]);
                    }

                    $subreq->merge(['__pagination' => false]);
                    $arr[$model] = $repo->all($subreq);
                }
            }
        }
    }

    /**
     * Заполняет настройками
     *
     * @param  array  &$arr
     * @return void
     */
    protected function combineSettings(array &$arr)
    {
        $settings = $this->settings();
        $data = [];

        foreach ($this->settingsSections as $section_name) {
            $key = $section_name;

            if (array_key_exists($key, $this->settingsSectionAlias)) {
                $key = $this->settingsSectionAlias[$key];
            }

            $section = $settings->loadSection($section_name);

            if (array_key_exists($section_name, $this->settingsOnly)) {
                $only = [];

                if (count($this->settingsOnly[$section_name]) === 1) {
                    $only = $section->read($this->settingsOnly[$section_name][0]);
                } else {
                    foreach ($this->settingsOnly[$section_name] as $offset) {
                        $only[$offset] = $section->read($offset);
                    }
                }

                $data[$key] = $only;
            } else {
                $data[$key] = $section->readAll();
            }
        }

        foreach ($this->settingsMutators as $offset => $mutator) {
            try {
                Arr::set($data, $offset, call_user_func($mutator, Arr::get($data, $offset)));
            } catch (Exception $ex) {
            }
        }

        $arr['settings'] = $data;
    }
}
