<?php

namespace App\Models\Repositories\JsonHead;

/**
 * Основная JSON-выгрузка в шапку сайта
 */
class MainRepository extends Repository
{
    /**
     * Список классов репозиториев моделей
     *
     * @var array
     */
    protected $modelRepoClasses = [
        \App\Models\Repositories\SocialNetworkRepository::class,
        \App\Models\Repositories\JustHouseRepository::class,
        \App\Models\Repositories\StockHouseRepository::class,
    ];

    /**
     * Список названий секций для автовыгрузки
     *
     * @var array
     */
    protected $settingsSections = [
        'contact_info',
    ];

    /**
     * Список псевдонимов для названий секций настроек
     *
     * @var array
     */
    protected $settingsSectionAlias = [
        'contact_info' => 'contact',
    ];

    /**
     * Список отдельных ключей из настроек
     *
     * @var array
     */
    protected $settingsOnly = [
        'contact_info' => [
            'phone',
            'address',
            'site',
            'route',
        ],
    ];

    /**
     * Создает экземпляр репозитория
     */
    public function __construct()
    {
        $lines = function ($value) {
            return explode(PHP_EOL, $value);
        };

        $this->settingsMutators = [
            'contact.route.personal' => $lines,
            'contact.route.public' => $lines,
        ];
    }
}
