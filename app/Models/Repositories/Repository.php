<?php

namespace App\Models\Repositories;

use App\Business\Repositories\ModelRepository;
use Illuminate\Http\Request;
use ReflectionClass;

/**
 * Общая реализация репозитория моделей
 */
class Repository implements ModelRepository
{
    /**
     * Класс модели, представляемой в репозитории
     *
     * @var string
     */
    protected $modelClass;

    /**
     * Количество записей для пагинационной выборки всех записей модели
     *
     * @var int
     */
    protected $perPage = 25;

    /**
     * Создает новый экземпляр
     */
    public function __construct()
    {
        $reflection = new ReflectionClass(get_class($this));
        $parts = explode('_', snake_case($reflection->getShortName()));

        if (count($parts) > 1) {
            array_pop($parts);

            $this->modelClass = str_replace('/', '\\', dirname(str_replace('\\', '/', $reflection->getNamespaceName())))
                . '\\'
                . studly_case(implode('_', $parts));
        }
    }

    /**
     * Стартует новый запрос к модели
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function startQuery()
    {
        return $this->modelClass::query();
    }

    /**
     * Стартует запрос для выборки всех моделей
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function queryAll(Request $request = null)
    {
        return $this->startQuery();
    }

    /**
     * Возвращает все записи модели, или с пагинацией
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function all(Request $request = null)
    {
        if ($request && !boolval($request->input('__pagination', true))) {
            return $this->queryAll($request)->get();
        }

        return $this->perPage > 0
            ? $this->queryAll($request)->paginate($this->perPage)
            : $this->queryAll($request)->get();
    }

    /**
     * Возвращает запись модели по указанному значению первичного ключа
     *
     * @param  $key
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function one($key)
    {
        return $this->startQuery()->findOrFail($key);
    }

    /**
     * Создает новую запись в моделе, возвращает ее экземпляр
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(Request $request)
    {
        return $this->modelClass::create($request->all());
    }

    /**
     * Обновляет данные записи модели, возвращает ее экземпляр
     *
     * @param  $key
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($key, Request $request)
    {
        $model = $this->one($key);
        $model->fill($request->all())->save();

        return $model;
    }

    /**
     * Удаляет запись модели
     *
     * @param  $key
     * @return bool|null
     */
    public function destroy($key)
    {
        return $this->one($key)->delete();
    }
}
