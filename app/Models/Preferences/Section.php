<?php

namespace App\Models\Preferences;

use App\Business\Data\Preferences\Section as SectionContract;
use Illuminate\Support\Arr;
use ReflectionClass;

/**
 * Реализация базиса у любой секции настроек
 */
abstract class Section implements SectionContract
{
    /**
     * Название ключа секции
     *
     * @var string
     */
    private $sectionKey;

    /**
     * Набор значений по умолчанию
     *
     * @var array
     */
    protected $defaults = [];

    /**
     * Значения секции
     *
     * @var array
     */
    private $values;

    /**
     * Создает экземпляр, прокидывая первоначальные значения секции
     *
     * @param  $data
     */
    public function __construct($data)
    {
        $reflection = new ReflectionClass(get_class($this));
        $parts = explode('_', snake_case($reflection->getShortName()));

        array_pop($parts);
        $this->sectionKey = implode('_', $parts);

        if (!is_array($data)) {
            $data = json_decode($data, true);
        }

        $this->values = is_array($data) ? $data : [];
    }

    /**
     * Возвращает название ключа секции
     *
     * @return string
     */
    final public function getSectionKey()
    {
        return $this->sectionKey;
    }

    /**
     * Собирает все значения секции в одну сериализованную строку
     *
     * @return string
     */
    final public function compile()
    {
        return json_encode($this->values, JSON_UNESCAPED_UNICODE);
    }

    /**
     * Считывает значение из секции по указанному адресу
     *
     * @param  string  $offset
     * @return mixed
     */
    final public function read(string $offset)
    {
        if (Arr::has($this->values, $offset)) {
            $value = Arr::get($this->values, $offset);

            if (is_array($value)) {
                $default = Arr::get($this->defaults, $offset);

                if (is_array($default)) {
                    return array_merge($default, $value);
                }
            }

            return $value;
        }

        return Arr::get($this->defaults, $offset);
    }

    /**
     * Считывает все значения из секции
     *
     * @return array
     */
    final public function readAll()
    {
        return array_merge($this->defaults, $this->values);
    }

    /**
     * Записывает значение в секцию по указанному адресу
     *
     * @param  string  $offset
     * @param  $value
     * @return static
     */
    final public function write(string $offset, $value)
    {
        Arr::set($this->values, $offset, $value);

        return $this;
    }

    /**
     * Записывает значения в секцию из массива
     *
     * @param  array  $data
     * @return static
     */
    final public function fill(array $data)
    {
        $this->fillRecursively($data, '');

        return $this;
    }

    /**
     * Удаляет значение из секции
     *
     * @param  string  $offset
     * @return static
     */
    final public function remove(string $offset)
    {
        Arr::forget($this->values, $offset);

        return $this;
    }

    /**
     * Запись значений в секцию из массива рекурсивно
     *
     * @param  array  $data
     * @param  string  $offset
     * @return void
     */
    private function fillRecursively(array $data, string $offset)
    {
        foreach ($data as $k => $v) {
            $new_offset = empty($offset) ? $k : $offset . '.' . $k;

            if (is_array($v)) {
                $this->fillRecursively($v, $new_offset);
            } elseif (Arr::has($this->defaults, $new_offset)) {
                $this->write($new_offset, $v);
            }
        }
    }
}
