<?php

namespace App\Models\Preferences;

/**
 * Секция настроек для вспомогательных списков слов / строк
 */
class HintsSection extends Section
{
    /**
     * Набор значений по умолчанию
     *
     * @var array
     */
    protected $defaults = [
        'decorations' => [],
        'technologies' => [],
    ];
}
