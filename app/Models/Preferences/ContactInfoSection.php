<?php

namespace App\Models\Preferences;

/**
 * Секция настроек для контактных данных
 */
class ContactInfoSection extends Section
{
    /**
     * Набор значений по умолчанию
     *
     * @var array
     */
    protected $defaults = [
        'email' => 'email',
        'phone' => 'phone',
        'address' => 'address',
        'site' => 'site',
        'route' => [
            'personal' => "personal\nroute",
            'public' => "public\nroute",
        ],
    ];
}
