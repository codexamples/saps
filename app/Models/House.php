<?php

namespace App\Models;

use App\Business\Data\Fields\FieldAccess;
use App\Business\Data\Fields\FileField;
use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    use FieldAccess, FileField;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'houses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'stock',
        'title',
        'text',
        'image',
        'decoration',
        'area',
        'technology',
        'rooms',
        'position_desktop',
        'position_mobile',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'stock',
        'text',
        'image',
        'created_at',
        'updated_at',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'text_lines',
        'image_url',
    ];

    /**
     * Возвращает текст, как массив строк
     *
     * @return array
     */
    public function getTextLinesAttribute()
    {
        return explode(PHP_EOL, $this->getFieldValue('text'));
    }

    /**
     * Возвращает ссылку на изображение
     *
     * @return string
     */
    public function getImageUrlAttribute()
    {
        return $this->getFileUrl('image');
    }
}
