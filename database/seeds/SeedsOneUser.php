<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

trait SeedsOneUser
{
    /**
     * Добавляет пользователя с указанными данными, а если есть, то ничего не делает
     *
     * @param  string  $name
     * @param  string  $email
     * @param  string  $password
     * @return void
     */
    protected function seedOneUser(string $name, string $email, string $password)
    {
        $user = DB::table('users')->where('name', $name)->first();

        if ($user) {
            return;
        }

        DB::table('users')->insert([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password),
        ]);
    }
}
