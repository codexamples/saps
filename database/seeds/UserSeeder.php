<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    use SeedsOneUser;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedOneUser('user', 'user@user.user', 'user123');
    }
}
