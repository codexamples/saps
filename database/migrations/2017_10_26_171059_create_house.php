<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHouse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('houses', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('stock');
            $table->string('title');
            $table->text('text');
            $table->string('image');
            $table->string('decoration');
            $table->float('area');
            $table->string('technology');
            $table->unsignedTinyInteger('rooms');
            $table->string('position_mobile')->nullable();
            $table->string('position_desktop')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('houses');
    }
}
