window.transliteration = {
    dict: {
        'А': 'A',
        'Б': 'B',
        'В': 'V',
        'Г': 'G',
        'Д': 'D',
        'Е': 'E',
        'Ё': 'YO',
        'Ж': 'ZH',
        'З': 'Z',
        'И': 'I',
        'Й': 'J',
        'К': 'K',
        'Л': 'L',
        'М': 'M',
        'Н': 'N',
        'О': 'O',
        'П': 'P',
        'Р': 'R',
        'С': 'S',
        'Т': 'T',
        'У': 'U',
        'Ф': 'F',
        'Х': 'KH',
        'Ц': 'TS',
        'Ч': 'CH',
        'Ш': 'SH',
        'Щ': 'SCH',
        'Ы': 'Y',
        'Э': 'E',
        'Ю': 'YU',
        'Я': 'YA',
        ' ': '-'
    },
    origin: [
        'A',
        'B',
        'C',
        'D',
        'E',
        'F',
        'G',
        'H',
        'I',
        'J',
        'K',
        'L',
        'M',
        'N',
        'O',
        'P',
        'Q',
        'R',
        'S',
        'T',
        'U',
        'V',
        'W',
        'X',
        'Y',
        'Z',
        '-',
        '_',
        '0',
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9'
    ],
    translate: function(src, dst) {
        var name = document.getElementById(src).value.split('');
        var hash = [];

        if (name.length > 0) {
            name.map(function(ch) {
                var uCh = ch.toUpperCase();

                if (window.transliteration.dict.hasOwnProperty(uCh)) {
                    hash.push(
                        ch === uCh ? window.transliteration.dict[uCh] : window.transliteration.dict[uCh].toLowerCase()
                    );
                } else if (window.transliteration.origin.indexOf(uCh) >= 0) {
                    hash.push(
                        ch === uCh ? uCh : uCh.toLowerCase()
                    );
                }
            });
        } else {
            for (var i = 0; i < 8; i++) {
                var ch = window.transliteration.origin[Math.floor(Math.random() * window.transliteration.origin.length)];
                hash.push(Math.random() > .5 ? ch : ch.toLowerCase());
            }
        }

        document.getElementById(dst).value = hash.join('');

        return false;
    }
};
