previewModal = {};

previewModal.start = function(prefix, onPreview) {
    $('.' + prefix + '-btn').each(function (index, item) {
        item = $(item);

        var data = item.data('preview');

        item.on('click', function () {
            $('#' + prefix + '-body').html(previewModal.getPreview(data, onPreview));
            $('#' + prefix + '-box').modal('show');
        });
    });
};

previewModal.getPreview = function(data, onPreview) {
    var arr = data ? onPreview(data) : [];

    return arr.length > 0 ? arr.join('') : '<i>&lt; Нет &gt;</i>';
};
