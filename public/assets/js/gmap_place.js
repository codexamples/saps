gmapPlace = {};

gmapPlace.init = function(idPrefix) {
    var pos = {
        lat: parseFloat($('#' + idPrefix + '--lat').val()),
        lng: parseFloat($('#' + idPrefix + '--lng').val())
    };

    var map = new GMaps({
        div: '#' + idPrefix,
        center: pos,
        zoom: 15,
        click: function(e) {
            map.removeMarkers();
            map.addMarker({
                position: e.latLng
            });

            $('#' + idPrefix + '--lat').val(e.latLng.lat());
            $('#' + idPrefix + '--lng').val(e.latLng.lng());
        }
    });

    map.addMarker({
        lat: pos.lat,
        lng: pos.lng
    });
};
