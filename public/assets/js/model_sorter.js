modelSorter = {};

modelSorter.start = function(data, csrf) {
    $('#sorter-form').on('submit', function() {
        var form = '<form method="POST" action="' + $(this).prop('action') + '">' +
            '<input type="hidden" name="_token" value="' + csrf + '">';

        $('#sorter-fields .form-group .key-field').each(function(index, element) {
            form += '<input type="hidden" name="order[]" value="' + $(element).data('key') + '">';
        });

        form = $(form + '</form>');
        $('body').append(form);
        form.submit();

        return false;
    });

    data.map(function(item) {
        $('#sorter-fields').append(
            '<div class="form-group">' +
            /**/'<div class="col-md-4">' +
            /****/'<input' +
            /********/' type="text"' +
            /********/' class="form-control key-field"' +
            /********/' value="' + item.key.field + ' = ' + item.key.value + '"' +
            /********/' data-key="' + item.key.value + '"' +
            /********/' readonly' +
            /********/'>' +
            /**/'</div>' +
            /**/'<div class="col-md-4">' +
            /****/'<input' +
            /********/' type="text"' +
            /********/' class="form-control title-field"' +
            /********/' value="' + item.title.field + ' = ' + item.title.value + '"' +
            /********/' readonly' +
            /********/'>' +
            /**/'</div>' +
            /**/'<div class="col-md-2">' +
            /****/'<button class="btn btn-block btn-primary"><i class="fa fa-caret-square-o-up"></i></button>' +
            /**/'</div>' +
            /**/'<div class="col-md-2">' +
            /****/'<button class="btn btn-block btn-warning"><i class="fa fa-caret-square-o-down"></i></button>' +
            /**/'</div>' +
            /**/'<div class="clearfix"></div>' +
            '</div>'
        );
    });

    $('#sorter-fields .form-group:first-child .btn-primary').prop('disabled', true);
    $('#sorter-fields .form-group:last-child .btn-warning').prop('disabled', true);

    $('#sorter-fields .form-group .btn-primary').on('click', function() {
        modelSorter.swapRows(
            $(this).parent().parent(),
            $(this).parent().parent().prev()
        );

        return false;
    });

    $('#sorter-fields .form-group .btn-warning').on('click', function() {
        modelSorter.swapRows(
            $(this).parent().parent(),
            $(this).parent().parent().next()
        );

        return false;
    });
};

modelSorter.swapRows = function(a, b) {
    a.fadeOut(400);
    b.fadeOut(400, function() {
        modelSorter.swapFields(a.find('.key-field'), b.find('.key-field'), true);
        modelSorter.swapFields(a.find('.title-field'), b.find('.title-field'), false);

        a.fadeIn(200);
        b.fadeIn(200);
    });
};

modelSorter.swapFields = function(a, b, keyAttr) {
    var tmp = a.val();

    a.val(b.val());
    b.val(tmp);

    if (keyAttr) {
        tmp = a.data('key');
        a.data('key', b.data('key'));
        b.data('key', tmp);
    }
};
